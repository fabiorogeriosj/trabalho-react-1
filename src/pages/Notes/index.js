import React, { Component } from 'react'
import { confirmAlert } from 'react-confirm-alert'; // Import
import 'react-confirm-alert/src/react-confirm-alert.css'; // Import css
import Header from '../../components/Header'

import { listNotes, saveNotes, removeNote, saveEditNoteService } from '../../services/serviceNotes'

import './style.css'

export default class Notes extends Component {
  
  state = {
    notes: [],
    notesNotFiltered: [],
    loading: true,
    loadingSave: false,
    search: '',
    text: ''
  }

  async componentDidMount () {
    this.setState({
      notesNotFiltered: await listNotes(),
      loading: false
    })
    this.filter()
  }

  newNote = () => {
    this.setState({
      new: true
    })
  }

  editText = (event) => {
    this.setState({
      text: event.target.value
    })
  }

  saveNote = async () => {
    this.setState({
      loadingSave: true
    })
    await saveNotes(this.state.text)
    this.setState({
      notes: await listNotes(),
      loadingSave: false,
      new: false,
      text: ''
    })
  }

  keyPress = (event) => {
    if(event.key === 'Enter') {
      this.saveNote()
    }
  }

  setFilter = (event) => {
    this.setState({
      search: event.target.value
    })
    this.filter(event.target.value)
  }

  filter = (value) => {
    value = !value ? this.state.search : value
    this.setState({
      notes: this.state.notesNotFiltered.filter(n => n.text.toUpperCase().indexOf(value.toUpperCase()) !== -1)
    })
  }

  editNote = (event) => {
    const id = event.target.id
    const index = this.state.notes.map(n => n.id.toString()).indexOf(id)
    const notes = this.state.notes
    notes[index].editing = true
    this.setState({
      notes: notes
    })

  }

  saveEditNote = async (event) => {
    const id = event.target.id
    const index = this.state.notes.map(n => n.id.toString()).indexOf(id)
    const notes = this.state.notes
    notes[index].editing = false
    this.setState({
      notes: notes
    })
    await saveEditNoteService(id, this.state.text)
    this.componentDidMount()
  }

  delNote = (event) => {
    const id = event.target.id
    confirmAlert({
      title: 'Veja bem...',
      message: 'Deseja realmente excluir essa nota?',
      buttons: [
        {
          label: 'Sim',
          onClick: async () => {
            await removeNote(id)
            this.componentDidMount()
          }
        },
        {
          label: 'Não'
        }
      ]
    });
  }
  render() {
    return (
      <div>
        <Header/>
        <div className='content'>
          
          <div className='filters'>
            <div className='left'>
              <input type='search' placeholder='Procurar...' value={this.state.search} onChange={this.setFilter} />
            </div>
            <div className='right'>
              <button onClick={this.newNote}>Nova nota</button>
            </div>
          </div>

          <hr />

          <div className='list'>
            { this.state.loading && (
              <div className='item item-empty'>
                Carregando anotações...
              </div>
            ) }
            { !this.state.notes.length && !this.state.loading && (
              <div className='item item-empty'>
                Nenhuma nota criada até o momento!
              </div>
            ) }
            { this.state.new && (
              <div className='item'>
                <input disabled={this.state.loadingSave} onKeyPress={this.keyPress} autoFocus onChange={this.editText} className='left' type='text' placeholder='Digite sua anotação' />
                <div className='right'>
                  { !this.state.loadingSave && (<button onClick={this.saveNote}>Salvar</button>) }
                  { this.state.loadingSave && (<span>Salvando...</span>) }
                </div>
              </div>
            )}
            { this.state.notes.map(note => (
              <div key={note.id} className='item'>
                <input onChange={this.editText} className='left' type='text' readOnly={!note.editing} defaultValue={note.text} placeholder='Digite sua anotação' />
                <div className='right'>
                  { (!note.id || note.editing) && (<button onClick={this.saveEditNote} id={note.id}>Salvar</button>) }
                  { note.id && !note.editing && (<button id={note.id} onClick={this.delNote}>Excluir</button>) }
                  { note.id && !note.editing && (<button id={note.id} onClick={this.editNote}>Editar</button>) }
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    )
  }
}
