const result = [
    { text: 'Lavar o carro', id: 1 },
    { text: 'Dar vacina na Alice', id: 2 }
]

const listNotes = () => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve(result)
        }, 200)
    })
}


const saveNotes = (text) => {

    return new Promise((resolve, reject) => {
        setTimeout(() => {
            result.unshift({
                text: text,
                id: new Date().getTime()
            })
            resolve()
        }, 200)
    })
}

const removeNote = (id) => {
    
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const index = result.map(e => e.id.toString()).indexOf(id)
            delete result[index]
            resolve()
        }, 200)
    })
}

const saveEditNoteService = (id, text) => {
    
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            const index = result.map(e => e.id.toString()).indexOf(id)
            result[index].text = text
            console.log(result)
            resolve()
        }, 200)
    })
}

module.exports = {
    listNotes,
    saveNotes,
    removeNote,
    saveEditNoteService
}